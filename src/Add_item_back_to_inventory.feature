Feature: Add item back to inventory
 As a Product owner,
 I want to add items back to inventory when they are returned or exchanged  
 so that I can Track Invenatory
 
 
Scenario: Items returned for refund should be added to inventory
Given that a customer previously bought a black swaeter from me
And   I have three black sweaters in inventory
When they return the black sweater for a refund
Then I should have four sweaters in inventoey


Scenario: Exhanged items should be returned to inventory
Given that a customer previously bought a blue from me
And I have two blue garments in inventory
And three black garments in inventory
When they exchenge the blue garment for a black garment
Then I should have three blue garments in inventoey
And two black garments in inventory
And One black jeans in inventory
